/*
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.email.aws;

import java.util.Dictionary;
import java.util.Hashtable;

import org.junit.Before;
import org.junit.Test;
import org.osgi.service.cm.ConfigurationException;


public class AWSEmailServiceTest {
    
    private AWSEmailService m_awsEmailService;
    
    @Before
    public void setup() {
        m_awsEmailService = new AWSEmailService();
    }	

    @Test
    public void testCorrectConfigWithoutRegion() throws ConfigurationException {
        // Should not throw exception
        m_awsEmailService.updated(getProperties());
    }
 
    @Test(expected = ConfigurationException.class)
    public void testConfigWithMissingKey() throws ConfigurationException {
        Dictionary<String, String> properties = getProperties();
        properties.remove("aws-key");
        m_awsEmailService.updated(properties);
    }
 
    @Test(expected = ConfigurationException.class)
    public void testConfigWithMissingSecret() throws ConfigurationException {
        Dictionary<String, String> properties = getProperties();
        properties.remove("aws-secret");
        m_awsEmailService.updated(properties);
    }
    
    @Test(expected = ConfigurationException.class)
    public void testConfigWithRegion() throws ConfigurationException {
        Dictionary<String, String> properties = getProperties();
        properties.put("aws-region", "EU_WEST_1");
        m_awsEmailService.updated(properties);
    }

    @Test
    public void testConfigWithRegionWithOfficialAmazonName() throws ConfigurationException {
        Dictionary<String, String> properties = getProperties();
        properties.put("aws-region", "us-east-1");
        m_awsEmailService.updated(properties);
    }
    
    private Dictionary<String, String> getProperties() {
        Dictionary<String, String> props = new Hashtable<>();
        props.put("aws-secret", "secret!");
        props.put("aws-key", "key!");
        return props;
    }

}
